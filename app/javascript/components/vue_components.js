import Vue from "vue"

new Vue({
    el: '#app4',
    data: {
        todos: [
            {text: 'Learn JavaScript'},
            {text: 'Learn Vue'},
            {text: 'Build something awesome'}
        ]
    }
})

new Vue({
    el: "#app5",
    data: {
        message: 'Hello Vue.js!'
    },
    methods:
        {
            reverseMessage: function () {
                this.message = this.message.split('').reverse().join('')
            }
        }
})